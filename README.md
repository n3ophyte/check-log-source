###Check Log Source v2.0###
* 2.0 : Rework core using time range instead of using python to compute time delta
* 2.1 : added worker to command input

##Description##
Controlla lo stato delle log source e avvisa nel caso non si ricevono eventi per un dato periodo, ci sono 2 modalità:

1. core=master invia le notifiche 
2. core=agent restarta il processo di logstash nel caso in cui non ci sono più eventi

il throttling period indica per quanto tempo lo script resta silente anche se rilevasse la perdita di eventi dopo 
l'ultima notifica. E' suddiviso per sorgente ed è presente sia per le mail che per l'esecuzione del comando di riavvio
di logstash.
E' preferibile mettere una istanza dello script sul collettore o sonda dove sono present i logstash più importanti in 
modalità agent e uno sulle macchine che fungono da master dove è presente ElasticSearch in modalità Master.


##HOW TO install/requirments:##
* yum/apt-get install python-pip
* pip install requests, hashlib
* *ONLY IF AGENT*: export logstash_directory=$****HERE LOGSTASH BINARY PATH****$
* edit /etc/envirorment and insert value $logtash_directory=/logstash absolute binary path withotut last /
* wget https://bitbucket.org/lutech/check-log-source/get/master.zip or git clone https://bitbucket.org/lutech/check-log-source
* unzip & cd check-log-source
* mkdir cache
* mkdir config

##HOW TO Configure config.cfg.example:##

```
#!

index_prefix=logstash- (solo il prefisso dell'indice su cui querare nel caso in cui ci fossero più indici è necessario configurare più scripts)

index_day_check=3 (impostare quanti indici include nella ricerca 3 significa che da oggi include i 3 indici precedenti)

email_throttling=7200 (indica che dopo la prima notifica lo scripts non manda più mail per 7200 secondi)

command_throttling=7200 (indica che dopo il primo riavvio lo scripts non esegure più il comando 7200 secondi)

command_path= (indica il path completo dove si trova il comando da eseguire nel caso in cui venga usato restart_logstash.sh compreso nel repo)

throttling_path (è necessario creare la cartella cache all'interno della directory dello scripts e inserito qui il path assoluto, dove verranno salvati i file per mantenere la cache )

core=master (master -> manda mail di notifica , agent -> esegue un comando)

[SOURCE]

s1 = all:3600:none

* all : indica che esegure una ricerca su tutto l'indice _search?q=*
* 3600 : indica quanto tempo è configurato come RANGE nella query ovvero se non trova eventi da ora a 3600s fa allora manda notifica
* none : indica che non esegue nessun comando

s2 = checkpoint:none:3600:none

* checkpoint : è il _type su cui viene eseguita la query
* none : indica che oltre al type non viene eseguito nessun filtro nella query
* 3600 : sono i secondi per cui rileva la mancanza di eventi
* none : indica che non esegue nessun comando

s3 = cisco_asa:device_ip=test:300:sh restart_logstash.sh checkpoint 100m 2 /root/lutechScripts/logs/logstash-1.4.1

* cisco_asa : _type
* device_ip=test : indica che oltre al _type alla query viene aggiunto quel particolare filtro è utile per clienti che hanno diversi apparati dello stesso tipo che si distinguono per device_ip
* 300 : sono i secondi (minimi)
* sh restart_logstash.sh : indica l'esecuzione di un comando nel caso si rilevasse la mancanza di eventi

```

##HOW Works restart_logastash.sh:##

```sh restart_logstash.sh [config name of logstash without .conf] [Amount of RAM] [Worker number default 1] [PATH of logstash config file]```

esempio:

cisco_asa:device_ip=test:300:sh restart_logstash.sh checkpoint 1000m 2 /root/lutechScripts/logs/logstash-1.4.1


**ATTENTION**
inserire nel file /etc/envirorment la variabile d'ambiete $logstash_directory con il valore dei binari di logstash


**Usage Example for crontab every hour:** 


```* */1 * * * root python [ROOT_PATH]/check_source.py -c [ABSOLUTE_PATH]source.cfg```


**Enable Debugging (print stdout but not send email):**


```python check_source.py -c source.cfg --debug```


**HELP:**


```python check_source.py -h```

##TODO##
