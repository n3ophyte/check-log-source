__author__ = 'stefano'
#!/usr/bin/env python
#
# log soruce checker
# st3fan_01 at hotmail.com
#
#
# ----------------------------------------------------------------------------
# The MIT License
#
# Copyright 2015 @n3ophyte
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE

import requests, hashlib
import subprocess
from datetime import datetime, timedelta
import socket, sys, time, os
import ConfigParser, json, base64
from optparse import OptionParser

# Import smtplib for the actual sending function
import smtplib
# Import the email modules we'll need
from email.mime.text import MIMEText

# Config VAR
VERSION = "2.1"

#Config parser
Config = ConfigParser.ConfigParser()
SECTIONS = ['EMAIL', 'GENERAL','SOURCE'] #Keep ordering
USAGE = """
Read README.md for more information about how to setup config/config.cfg.example
"""
parser = OptionParser(USAGE)
parser.add_option("-c",
                  action="store", type="string", dest="config",
                  default = "",
                  help="load configuration file")
parser.add_option("-v",
                  action="store_true", dest="version",
                  default=False,
                  help="show version")
parser.add_option("--debug",
                  action="store_true", dest="debug",
                  default=False,
                  help="debug mode on")

(options, args) = parser.parse_args()

if options.version:
    print "L-TMS Source Checker Agent/Master ", VERSION
    sys.exit()

if not options.config:
    print "config file missing use option -c config file!"
    sys.exit()

# Global config Value
CONFIG = {}

# read options from config ( -c filename )
if options.config != "":
    if os.access(options.config,os.R_OK):
        Config.read(options.config)

        for section in Config.sections():
            if options.debug:
                print "Parsing", section
            if section not in SECTIONS:
                continue
            # IR and CUSTOMER Sections are dynamic
            if section == SECTIONS[2]:
                CONFIG[section] = []
                for option,value in Config.items(section):
                    if options.debug:
                        print "    ", value
                    # Special Handling for empty value
                    if value:
                        CONFIG[section].append(value)
                CONFIG[section].sort()
            else:
                # Special handling for descriptive config value
                CONFIG[section]={}
                for option,value in Config.items(section):
                    if options.debug:
                        print "    %s = %s" % (option,value)
                    CONFIG[section][option]=value
    else:
        print "Unable to load configuration file:", options.config
        print "Using default configuration"


def epoch():
    """
    :return:
    """
    return int(str(time.time()).split('.')[0])


def get_config_name():
    """


    :return: config file name without any word after .
    """
    config_name = str(options.config).split(".")[0]
    if "/" in config_name:
        config_name_last = config_name.split("/")
        return config_name_last[-1]
    else:
        return config_name


def throttling(subject,config_cache_value,cache_type,cache_path):
    """

    :param subject: unique message to execute cache against
    :param config_cache_value: config parser option of throttling disabled, empty or time in seconds
    :param cache_type: needed for separate the cache files name between different cache
    :param cache_path: directory where cache files are saved, default should be ./cache
    :return: True if email should not be sent beacuse of throrrling
    """
    if config_cache_value == "" or "disabled" in config_cache_value:
        return False

    if os.path.exists(cache_path+'/cache_'+cache_type+'_'+get_config_name()+'.json'):
        if options.debug:
            print 'save throttling for '+cache_type+' content '+subject
        file_cache_stats = os.stat(cache_path+'/cache_'+cache_type+'_'+get_config_name()+'.json')
        # purge cache file if it is too big
        # TODO: we should clean old email instead of remove all the cache file
        if int(file_cache_stats.st_size) > (1024*1024*10):
            try:
                os.remove(cache_path+'/cache_'+cache_type+'_'+get_config_name()+'.json')
            except OSError:
                pass
            return False
        data = json.load(open(cache_path+'/cache_'+cache_type+'_'+get_config_name()+'.json'))
        subject_hash = hashlib.md5(subject).hexdigest()
        if subject_hash in data:
            # TODO: not implemented yet!
            if CONFIG['GENERAL']['alertonlyonce'] == "enabled":
                return True
            else:
                # If the difference between now and the throttling time is greater then the timestamp saved
                # we need to reset the counter beacuse we are out of time limit
                if epoch() - int(config_cache_value) > int(data[subject_hash]['timestamp']):
                    # not in throttling so send mail
                    data[subject_hash]['timestamp'] = epoch()
                    json.dump(data, open(cache_path+'/cache_'+cache_type+'_'+get_config_name()+'.json','w'))
                    return False
                else:
                    # Throttling
                    return True
        else:
            data[subject_hash] = {}
            data[subject_hash]['subject'] = subject
            data[subject_hash]['timestamp'] = epoch()
            json.dump(data, open(cache_path+'/cache_'+cache_type+'_'+get_config_name()+'.json','w'))
            return False
    else:
        # first time the cache is created
        data = {hashlib.md5(subject).hexdigest():{'subject': subject, "timestamp":epoch()}}
        try:
            json.dump(data, open(cache_path+'/cache_'+cache_type+'_'+get_config_name()+'.json','w'))
        except Exception as inst:
            print "Failed to Write cache in "+cache_path
            if options.debug:
                print inst
        return False


def send_email(data,to,from_mail,subject):
    """

    :param data: body of email
    :param to: destination addresses
    :param from_mail: from mail address
    :param subject: subject of mail
    """
    if not subject:
        print "Email Subject Missing"
        sys.exit()
    msg = MIMEText(CONFIG[SECTIONS[0]]['body_header']+data)
    msg['Subject'] = CONFIG[SECTIONS[0]]['subject_header']+' '+subject
    #msg = MIMEMultipart()
    msg['From'] = from_mail

    if ',' in to:
        receiver = to.split(',')
        msg['To'] = ', '.join( receiver )
    else:
        msg['To'] = to
        receiver = to

    if options.debug and not throttling(subject,CONFIG['GENERAL']['email_throttling'], "email", CONFIG['GENERAL']['throttling_path']):
        print msg.as_string()
    else:
    # me == the sender's email address
    # you == the recipient's email address
    # Send the message via our own SMTP server, but don't include the
    # envelope header.
        #throttle = False
        if not throttling(subject,CONFIG['GENERAL']['email_throttling'], "email", CONFIG['GENERAL']['throttling_path']):
            s = smtplib.SMTP(CONFIG[SECTIONS[0]]['smtp'])
            s.sendmail(from_mail, receiver, msg.as_string())
            s.quit()
        else:
            if options.debug:
                print "Mail is throttling for "+CONFIG['GENERAL']['email_throttling']+"s\n f"+msg.as_string()
        #    throttle = True


def get_index(index_prefix):
    """


    :param index_prefix: Elasticsearch index prefix
    :return: index date of current day and yesterday exception if hour is from 00 and 02
    """
    i = 1
    index_values = ""
    while i <= int(CONFIG['GENERAL']['index_day_check']):
        index_values += index_prefix+(datetime.utcfromtimestamp(time.time()) - timedelta(days=i)).strftime("%Y.%m.%d")+","
        i = i + 1

    #yesterday = datetime.utcfromtimestamp(time.time()) - timedelta(days=1)
    #yesterday_index = yesterday.strftime("%Y.%m.%d")
    today_index = (datetime.utcfromtimestamp(time.time())).strftime("%Y.%m.%d")
    index_values += index_prefix+today_index
    if options.debug:
        print "Print Current Search Index: "
        print index_values
    return index_values


def check_ping(string_ip,proxies):
     """

     :param string_ip: elasticsearch node to where execute query
     :return: 0 if node is not reachable
     """
     try:
          r = requests.get(string_ip,proxies=proxies)
     except requests.exceptions.Timeout:
    # Maybe set up for a retry, or continue in a retry loop
          return 0
     except requests.exceptions.ConnectionError:
    # Maybe set up for a retry, or continue in a retry loop
          return 0
     except requests.exceptions.RequestException as e:
    # catastrophic error. bail.
          return 0
     del r
     return 1


def execute_command(bash_script):
    """

    :param bash_script: .sh scripts that should be execute if no events where found for specific source
    :return: False if command has to be throttle or it is disabled
    """
    if "none" in bash_script or throttling(bash_script,CONFIG['GENERAL']['command_throttling'], "command", CONFIG['GENERAL']['throttling_path']):
        return False
    # It means that there are arguments
    if " " in bash_script:
        bash_argument = bash_script.split(" ")
        bash_argument[1] = CONFIG['GENERAL']['command_path']+bash_argument[1]
        bash_argument[0] = bash_argument[0]
        print bash_argument
        p = subprocess.Popen(bash_argument)
        #output, err = p.communicate()
        print "*** Running bash script *** with pid", p.pid
        return True
    else:
        p = subprocess.Popen(CONFIG['GENERAL']['command_path']+bash_script)
        print "*** Running bash script *** with pid: ", p.pid
        return True


def check_source_input(source_array_options):

    """

    :param source_array_options: array of source options splitted from :
    :return: packed array
    """
    new_source_array_options = ["none","none","none","none"]
    # Addressing version priors to 2.1
    if len(source_array_options) == 3:
        # shift array position 1 because of field query in 2.1
        new_source_array_options[0] = source_array_options[0]
        new_source_array_options[1] = "none"
        new_source_array_options[2] = source_array_options[1]
        new_source_array_options[3] = source_array_options[2]
    else:
        new_source_array_options = source_array_options

    # check if throttling time ti too big
    if int(new_source_array_options[2]) >= 150000:
        print "Error: throttling time for "+new_source_array_options[0]+" is too big ("+new_source_array_options[2]+"), should be < of 150.000s"
        sys.exit(0)
    if int(new_source_array_options[2]) < 300:
        print "Error: throttling time for "+new_source_array_options[0]+" is too short ("+new_source_array_options[2]+"), should be > 300s"
        sys.exit(0)
    # space in document type
    new_source_array_options[0] = new_source_array_options[0].strip()
    if not new_source_array_options[0]:
        print "Error: document type empty"
        sys.exit(0)
    # prevent stupid command
    # TODO: add dictionary list of prohibited commands
    # TODO: obv by running a  bash script would mean that all these checks arent nedded beacuse it will be bypassed
    if "rm -rf" in new_source_array_options[3] or "shutdown" in new_source_array_options[3]:
        sys.exit(0)
    # TODO: check command input for help user to complete it

    # From version 2.1 worker of logstash restart command
    if "sh restart_logstash.sh" in new_source_array_options[3] and len(new_source_array_options[3].split(" ")) <= 5:
        # no worker beacuse of previus version
        if options.debug:
            print "New version "+VERSION+" need worker to be set in args 3 instead of path that is position 4"
        temp_new_source_array_options = new_source_array_options[3].split(" ")
        new_source_array_options[3] = temp_new_source_array_options[0] +" " +temp_new_source_array_options[1] + " " + temp_new_source_array_options[2] +" "+ temp_new_source_array_options[3] +" 1 "+ temp_new_source_array_options[4]

    return new_source_array_options


# Override Proxy
if "proxy" in CONFIG['GENERAL'] and CONFIG['GENERAL']['proxy'] == "none":
    proxies = {
        'no': 'pass',
    }
elif "proxy" in CONFIG['GENERAL']:
    proxies = {
        'http': CONFIG['GENERAL']['proxy']
    }
else:
    proxies = {
        'no': 'pass',
    }
# Override ENV variables because requests use proxy in UNIX ENV
elastic_ip = CONFIG['GENERAL']['elasticsearch'].split("//")[1]
if "/" in elastic_ip:
    elastic_ip = elastic_ip[:-1]
os.environ['no_proxy'] = '127.0.0.1,localhost,'+elastic_ip

# ES Cluster IP or reverse proxy
string_ip = CONFIG['GENERAL']['elasticsearch']


if check_ping(string_ip,proxies) == 1:
    for source in CONFIG['SOURCE']:
        if ":" in source:
            # 0 is the source name
            # 1 is the specific query search field key=value if needed otherwise none
            # 2 is the time span to check for last event
            # 3 is the command to execute if 1 return ok if no command is set value is none
            # that there is no events
            source_options = check_source_input(source.split(":"))

        else:
            print "Error in Source configuration : is missing.."
            sys.exit()
        if "all" in source_options[0]:
            # if s = all: there shouldn't be a field to match!
            if "none" not in source_options[1]:
                print "All type source option can't have a specific query field please put none"
                sys.exit(0)
            if CONFIG['GENERAL']['user']:
                last_event = requests.get(string_ip+"/"+",".join(get_index(CONFIG['GENERAL']['index_prefix']))+"/_search?q=@timestamp:[\"now-"+source_options[2]+"s\"+TO+\"now\"]&sort=@timestamp:desc&size=1&pretty",auth=(CONFIG['GENERAL']['user'], CONFIG['GENERAL']['password']),proxies=proxies).json()
            else:
                last_event = requests.get(string_ip+"/"+",".join(get_index(CONFIG['GENERAL']['index_prefix']))+"/_search?q=@timestamp:[\"now-"+source_options[2]+"s\"+TO+\"now\"]&sort=@timestamp:desc&size=1&pretty",proxies=proxies).json()
            if "error" in last_event and "IndexMissingException" in last_event['error']:
                # TODO: is this real needed ? or should the get_index method fix it?
                # skip index delay creation at 02 AM
                if int(time.strftime("%H")) > 03 and int(time.strftime("%H")) <= 23:
                    if options.debug:
                        print "current hour index checking: "+str(time.strftime("%H"))
                    # TODO: add body of email with some help on how to solve the problem explained in the subject
                    # Master mean that this is the main process that check for every source instead of run it on
                    # every collector core = agent so it can do remediation like restart LS
                    if CONFIG['GENERAL']['core'] == "master":
                        send_email(" ", CONFIG['EMAIL']['to'],CONFIG['EMAIL']['from'],"CRITICAL for "+string_ip+" Today Index is missing")
                    continue
            # if no mapping is found for @timestamp it cloud mean that there arent events in index or the source name is wrong!
            if "error" in last_event and "SearchPhaseExecutionException" in last_event['error']:
                if CONFIG['GENERAL']['core'] == "master":
                    # Custom email subject because of same source name but different query filed
                    if "none" not in source_options[1]:
                        email_subject = source_options[0]+" "+source_options[1]
                    else:
                        email_subject = source_options[0]
                    send_email(" ", CONFIG['EMAIL']['to'],CONFIG['EMAIL']['from'],"CRITICAL we do not receive "+email_subject+" events for "+source_options[2]+" s")
                elif CONFIG['GENERAL']['core'] == "agent":
                    # TODO: here security issues!!!!
                    if execute_command(source_options[3]):
                        if options.debug:
                            print "Execute command for "+source_options[0]
                        # TODO: SEND SYSLOG MESSAGES
                        if options.debug:
                            print "Send local Syslog Messages: "
                continue
        else:
            if not "none" in source_options[1]:
                fields_query = source_options[1].split("=")
                #TODO here we have to check the number of fields! now only one key=value is supported!
                field_query = fields_query[0]+":"+fields_query[1]
            else:
                field_query = "*"
            if CONFIG['GENERAL']['user']:
                last_event = requests.get(string_ip+"/"+",".join(get_index(CONFIG['GENERAL']['index_prefix']))+"/"+source_options[0]+"/_search?q="+field_query+" AND @timestamp:[\"now-"+source_options[2]+"s\"+TO+\"now\"]&sort=@timestamp:desc&size=1&pretty",auth=(CONFIG['GENERAL']['user'], CONFIG['GENERAL']['password']),proxies=proxies).json()
            else:
                last_event = requests.get(string_ip+"/"+",".join(get_index(CONFIG['GENERAL']['index_prefix']))+"/"+source_options[0]+"/_search?q="+field_query+" AND @timestamp:[\"now-"+source_options[2]+"s\"+TO+\"now\"]&sort=@timestamp:desc&size=1&pretty",proxies=proxies).json()
            #if options.debug:
            #    print last_event
            if "error" in last_event and "IndexMissingException" in last_event['error']:
                if int(time.strftime("%H")) > 03 and int(time.strftime("%H")) <= 23:
                    if options.debug:
                        print "current hour index checking: "+str(time.strftime("%H"))
                    # TODO: add body of email with some help on how to solve the problem explained in the subject
                    if CONFIG['GENERAL']['core'] == "master":
                        send_email(" ", CONFIG['EMAIL']['to'],CONFIG['EMAIL']['from'],"CRITICAL for "+string_ip+" Today Index is missing")
                    elif CONFIG['GENERAL']['core'] == "agent":
                        # TODO: here security issues!!!!
                        if execute_command(source_options[3]):
                            if options.debug:
                                print "Execute command for "+source_options[0]
                            # TODO: SEND SYSLOG MESSAGES
                            if options.debug:
                                print "Send local Syslog Messages: "
                    continue
            # if no mapping is found for @timestamp it cloud mean that there arnt events in index or the source name is wrong!
            if "error" in last_event and "SearchPhaseExecutionException" in last_event['error']:
                if CONFIG['GENERAL']['core'] == "master":
                    # Custom email subject because of same source name but different query filed
                    if "none" not in source_options[1]:
                        email_subject = source_options[0]+" "+source_options[1]
                    else:
                        email_subject = source_options[0]
                    send_email(" ", CONFIG['EMAIL']['to'],CONFIG['EMAIL']['from'],"CRITICAL we do not receive "+email_subject+" events for "+source_options[2]+" s")
                elif CONFIG['GENERAL']['core'] == "agent":
                    # TODO: here security issues!!!!
                    if execute_command(source_options[3]):
                        if options.debug:
                            print "Execute command for "+source_options[0]
                        # TODO: SEND SYSLOG MESSAGES
                        if options.debug:
                            print "Send local Syslog Messages: "
                continue
        if options.debug:
            print " Source: "+source_options[0]+" ---- Result of count: "+str(last_event['hits']['hits'])
        # If total of json result is 0 it means that we have no events in db!
        if int(last_event['hits']['total']) == 0:
            if CONFIG['GENERAL']['core'] == "master":
                # Custom email subject because of same source name but different query filed
                if "none" not in source_options[1]:
                    email_subject = source_options[0]+" "+source_options[1]
                else:
                    email_subject = source_options[0]
                send_email(" ", CONFIG['EMAIL']['to'],CONFIG['EMAIL']['from'],"CRITICAL we do not receive "+email_subject+" events for "+source_options[2]+" s")
            elif CONFIG['GENERAL']['core'] == "agent":
                # TODO: here security issues!!!!
                if execute_command(source_options[3]):
                    if options.debug:
                        print "Execute command for "+source_options[0]
                    # TODO: SEND SYSLOG MESSAGES
                    if options.debug:
                        print "Send local Syslog Messages: "
        #sys.exit(0) #### HERE ####
else:
    print "Error no connection to "+string_ip