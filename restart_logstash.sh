#!/bin/sh
# COMMAND ARGS: [logastash config name before .] [RAM of logstash process] [path of logstash config without the last slash]
# /var/log/logstash directory must be created and own by the user that execute script

worker="1"

if [ ! -z "$3" ]; then
    worker=$3
fi

export LS_HEAP_SIZE=$2

cd $4

case "$(ps x |grep -v grep| grep -v nano | grep -v SCREEN |grep "$1.conf" | awk '{print $1}' | wc -w)" in

0) echo "Restarting process logstash-$1:     $(date) " >> /var/log/messages
   $logstash_directory/bin/logstash -f $4/$1.conf -w $worker --log /var/log/logstash/ls_$1.log  > /dev/null 2>&1 &
   ;;
1) kill -9 $(ps x |grep -v grep | grep -v nano | grep -v SCREEN |grep $1.conf | awk '{print $1}')
   echo "Restarting process logstash-$1:     $(date) " >> /var/log/messages
   $logstash_directory/bin/logstash -f $4/$1.conf -w $worker --log /var/log/logstash/ls_$1.log  &
   ;;
*) # More then one of same instance of logstash config what happen????
   ;;
esac

export LS_HEAP_SIZE=""

exit 0
